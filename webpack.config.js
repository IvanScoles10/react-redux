const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

const htmlWebPackPlugin = new HtmlWebPackPlugin({
    template: './public/index.html',
    filename: 'index.html'
});


module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname + '/dist', 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            // application
            {
                test: /\.(js|jsx)$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: {
                    presets: ['es2015', 'stage-2', 'react']
                }
            },
            // eslint
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'eslint-loader',
                query: {
                    presets: ['es2015', 'stage-2', 'react']
                }
            },
            {
                test: /\.(scss)$/,
                use: [{
                    // adds CSS to the DOM by injecting a `<style>` tag
                    loader: 'style-loader'
                }, {
                    // interprets `@import` and `url()` like `import/require()` and will resolve them
                    loader: 'css-loader'
                }, {
                    // loader for webpack to process CSS with PostCSS
                    loader: 'postcss-loader',
                    options: {
                      plugins: function () {
                        return [
                          require('autoprefixer')
                        ];
                      }
                    }
                }, {
                    // loads a SASS/SCSS file and compiles it to CSS
                    loader: 'sass-loader'
                }]
            }
        ]
    },
    plugins: [htmlWebPackPlugin],
    stats: {
        children: false,
        colors: true
    },
    devtool: 'source-map'
};
