import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import { get, extend } from 'lodash'
import { Link } from 'react-router-dom';

import './navigation-link.scss';

class NavigationLink extends React.Component {

    render () {
        return (
            <li className={this.getClass()}>
                <Link {...this.getLinkProps()}>
                    {this.props.children}
                </Link>
            </li>
        );
    }

    getClass () {
        return classNames(this.props.className, {
          'navigation-link': true,
          'navigation-link_active': (get(this.context, 'router.route.location.pathname')
              === this.props.to),
          'nav-item': true
        });
    }

    getLinkProps () {
        return extend(this.props, {
            className: 'nav-link'
        });
    }
}

NavigationLink.contextTypes = {
    router: PropTypes.object.isRequired
};

NavigationLink.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    to: PropTypes.string
};

export default NavigationLink;
