import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

class Button extends React.Component {

    static get defaultProps() {
        return {
            color: 'primary',
            outline: true,
            position: 'left',
            size: 'small',
            type: 'button'
        };
    }

    constructor () {
        super();

        this.handleButtonClick = this.handleButtonClick.bind(this);
    }

    render () {
        return (
            <button {...this.getButtonProps()}>
                 {this.props.children}
            </button>
        );
    }

    getClass () {
        let classes = classNames(this.props.className, this.getColorClass());
        let size = this.props.size;

        return classNames(classes, {
            'btn': true,
            'btn-block': (size === 'block'),
            'btn-sm': (size === 'small'),
            'btn-lg': (size === 'large'),
            'float-right': (this.props.position === 'right')
        });
    }

    getColorClass () {
        let buttonColor = {
            danger: 'btn-danger',
            dark: 'btn-dark',
            info: 'btn-info',
            light: 'btn-light',
            secondary: 'btn-secondary',
            success: 'btn-success',
            primary: 'btn-primary',
            warning: 'btn-warning'
        };

        if (this.props.outline) {
            buttonColor = {
                danger: 'btn-outline-danger',
                dark: 'btn-outline-dark',
                info: 'btn-outline-info',
                light: 'btn-outline-light',
                secondary: 'btn-outline-secondary',
                success: 'btn-outline-success',
                primary: 'btn-outline-primary',
                warning: 'btn-outline-warning'
            };
        }

        return buttonColor[this.props.color];
    }

    getButtonProps () {
         return {
             className: this.getClass(),
             id: this.props.id,
             onClick: this.handleButtonClick,
             size: 'button'
         };
    }

    handleButtonClick (event) {
        if (this.props.handleButtonClick) {
            this.props.handleButtonClick(event);
        }
    }
}

Button.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    color: PropTypes.oneOf([
       'danger',
       'dark',
       'info',
       'light',
       'secondary',
       'success',
       'primary',
       'warning'
    ]),
    handleButtonClick: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
    outline: PropTypes.bool,
    position: PropTypes.oneOf(['left', 'right']),
    size: PropTypes.oneOf(['block', 'large', 'small']),
    type: PropTypes.oneOf(['button', 'submit'])
};

export default Button;
