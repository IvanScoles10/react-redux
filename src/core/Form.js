import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

class Form extends React.Component {

    constructor () {
        super();

        this.state = {
             isValidated: false
        };

        this.submitHandler = this.submitHandler.bind(this);
    }

    componentDidMount () {
        // refactor move this function to helpers
        const formLength = this.formEl.length;

        for (let i = 0; i < formLength; i++) {
            const node = this.formEl[i];
            const label = node.parentNode.querySelector('.label-' + node.id);

            if (label && node.nodeName.toLowerCase() !== 'button') {
                if (node.required) {
                   label.innerHTML += ' *';
                }
            }
        }
    }

    render () {
        return (
            <form {...this.getFormProps()}>
                {this.props.children}
            </form>
        );
    }

    getFormProps () {
        return {
            className: this.getClass(),
            noValidate: true,
            onSubmit: this.submitHandler,
            ref: form => this.formEl = form
        };
    }

    getClass () {
        return classNames(this.props.className, {
            'form': true,
            'form--validated': this.state.isValidated
        })
    }

    validate () {
        const formLength = this.formEl.length;
        // same refactor move this function to helpers
        if (this.formEl.checkValidity() === false) {
            for (let i=0; i < formLength; i++) {
                const node = this.formEl[i];
                const feedback = node.parentNode
                    .querySelector('.feedback-' + node.id);

                if (feedback && node.nodeName.toLowerCase() !== 'button') {
                    let feedbackMessage = document.createElement('div');
                    feedback.innerHTML = '';

                    if (!node.validity.valid) {
                        // append element to form control and show message
                        feedbackMessage.innerHTML = 'Looks wrong!';
                        feedback.classList.add('invalid-feedback');
                        feedback.appendChild(feedbackMessage);
                    } else {
                        feedbackMessage.classList.add('valid-feedback');
                        feedbackMessage.innerHTML = 'Looks good!';
                        feedback.appendChild(feedbackMessage);
                    }
                }
            }

            return false;
        } else {
            for (let i=0; i < formLength; i++) {
                const elem = this.formEl[i];
                const errorLabel = elem.parentNode.querySelector('.invalid-feedback');

                if (errorLabel && elem.nodeName.toLowerCase() !== 'button') {
                    errorLabel.textContent = '';
                }
            }

            return true;
       }
    }

    submitHandler (event) {
        event.preventDefault();

        if (this.validate()) {
            this.props.submit();
        }

        this.setState({
            isValidated: true
        });
    }
}

Form.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    submit: PropTypes.func.isRequired
};

export default Form;
