import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

class Form extends React.Component {

    constructor () {
        super();

        this.state = {
             isValidated: false
        };

        this.submitHandler = this.submitHandler.bind(this);
    }

    componentDidMount () {
        // refactor move this function to helpers
        const formLength = this.formEl.length;

        for (let i = 0; i < formLength; i++) {
            const node = this.formEl[i];
            const label = node.parentNode.querySelector('.label-' + node.id);

            if (label && node.nodeName.toLowerCase() !== 'button') {
                if (node.required) {
                   label.innerHTML += ' *';
                }
            }
        }
    }

    render () {
        return (
            <form {...this.getFormProps()}>
                {this.props.children}
            </form>
        );
    }

    getFormProps () {
        return {
            className: this.getClass(),
            noValidate: true,
            onSubmit: this.submitHandler,
            ref: form => this.formEl = form
        };
    }

    getClass () {
        return classNames(this.props.className, {
            'form': true,
            'form--validated': this.state.isValidated
        })
    }

    validate () {
        const formLength = this.formEl.length;
        // same refactor move this function to helpers
        if (this.formEl.checkValidity() === false) {
            for (let i = 0; i < formLength; i++) {
                const node = this.formEl[i];
                const feedback = node.parentNode
                    .querySelector('.feedback-' + node.id);

                if (feedback && node.nodeName.toLowerCase() !== 'button') {
                    const input = node.parentNode
                        .querySelector('#' + node.id);
                    let feedbackMessage = document.createElement('div');
                    feedback.innerHTML = '';
                    this.clearMessages(input, feedback);

                    if (!node.validity.valid) {
                        // append element to form control and show message
                        input.classList.add('is-invalid');
                        feedback.classList.add('invalid-feedback');
                        feedbackMessage.innerHTML = 'Looks wrong!';
                        feedback.appendChild(feedbackMessage);
                    } else {
                        input.classList.add('is-valid');
                        feedback.classList.add('valid-feedback');
                        feedbackMessage.innerHTML = 'Looks good!';
                        feedback.appendChild(feedbackMessage);
                    }
                }
            }

            return false;
        } else {
            for (let i = 0; i < formLength; i++) {
                const node = this.formEl[i];
                const feedback = node.parentNode
                    .querySelector('.feedback-' + node.id);

                if (feedback && node.nodeName.toLowerCase() !== 'button') {
                    const input = node.parentNode.querySelector('#' + node.id);
                    this.clearMessages(input, feedback);
                    feedback.textContent = '';
                }
            }

            return true;
       }
    }

    clearMessages (input, feedback) {
        input.classList.remove('is-invalid');
        input.classList.remove('is-valid');
        feedback.classList.remove('invalid-feedback');
        feedback.classList.remove('valid-feedback');
    }

    submitHandler (event) {
        event.preventDefault();

        if (this.validate()) {
            this.props.submit();
        }

        this.setState({
            isValidated: true
        });
    }
}

Form.propTypes = {
    children: PropTypes.node.isRequired,
    className: PropTypes.string,
    submit: PropTypes.func.isRequired
};

export default Form;
