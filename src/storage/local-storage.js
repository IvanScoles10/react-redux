import { isEmpty } from 'lodash'
import moment from 'moment';
import stores from './store-config';
import date from '../helpers/date';

const isExpired = (expirationDate) => {
    return (moment(expirationDate, date.FULL_DATE_TIME_FORMAT)
        .isBefore(moment()));
};

export const loadState = () => {
    try {
        let state = {};
        let serializedState;

        stores.map(function (store) {
            let storeName = store.name + '-store-' + store.version;

            serializedState = (store.type === 'LOCAL') ? 
                localStorage.getItem(storeName) :
                sessionStorage.getItem(storeName);

            if (serializedState !== null) {
                let storeState = JSON.parse(serializedState);
            
                if (!isExpired(storeState.expirationDate)) {
                    state[store.name] = storeState.data;
                } else {
                    (store.type === 'LOCAL') ?
                        localStorage.removeItem(storeName) :
                        sessionStorage.removeItem(storeName);
                }
            }
        });

        if (isEmpty(state)) {
            return undefined;
        }

        return state;
    } catch (err) {
        console.log('error loadState:', err);

        return undefined;
    }
};

export const saveState = (state) => {
    try {
        let serializedState;

        stores.map(function (store) {
            let storeName = store.name + '-store-' + store.version;
            let storeState = {
                data: state[store.name],
                expirationDate: moment().add(store.timeToLive, 'minutes').format(date.FULL_DATE_TIME_FORMAT)
            };

            serializedState = JSON.stringify(storeState);

            (store.type === 'LOCAL') ? 
                localStorage.setItem(storeName, serializedState) : 
                sessionStorage.setItem(storeName, serializedState);
        });
    } catch (err) {
        console.log('error saveState:', err);
    }
};