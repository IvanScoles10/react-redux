/**
 * name: store name string (state)
 * timeToLive: store expiration number (minutes)
 * type: store type session or local storage LOCAL or SESSION
 * version: store version string
 */

export default [{
    name: 'users',
    timeToLive: 60,
    type: 'LOCAL',
    version: 'v1'
}];