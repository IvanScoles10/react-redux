import { createStore } from 'redux';
import reducer from '../reducers';
import { loadState, saveState } from './local-storage';

const persistedState = loadState();
const store = createStore(
    reducer, 
    persistedState
);

store.subscribe(function () {
    saveState(store.getState());
});

export default store;