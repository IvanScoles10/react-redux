import * as types from '../constants/action-types'
import * as actions from './index'

describe('user actions', () => {
  
    it('addUser should create ADD_USER action', () => {
        expect(actions.addUser({
          first_name: 'John',
          last_name: 'Doe'
        })).toEqual({
            type: types.ADD_USER,
            user: {
              first_name: 'John',
              last_name: 'Doe'
            }
        });
    });

    it('deleteUser should create DELETE_USER action', () => {
        expect(actions.deleteUser(1)).toEqual({
            type: types.DELETE_USER,
            id: 1
        });
    });

    it('editUser should create EDIT_USER action', () => {
        expect(actions.editUser(1, {
          first_name: 'John',
          last_name: 'Does'
        })).toEqual({
            id: 1,
            type: types.EDIT_USER,
            user: {
              first_name: 'John',
              last_name: 'Does'
            }
        });
    });
})
