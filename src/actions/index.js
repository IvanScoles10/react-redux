import * as actionTypes from '../constants/action-types'

// USER TYPES
export const addUser = user => ({ type: actionTypes.ADD_USER, user });
export const deleteUser = id => ({ type: actionTypes.DELETE_USER, id });
export const editUser = (id, user) => ({ type: actionTypes.EDIT_USER, id, user });