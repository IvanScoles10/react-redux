export default Object.freeze({
    FULL_DATE_TIME_FORMAT: 'YYYY-MM-DD HH:mm:ss'
});
