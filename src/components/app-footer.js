import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

class AppFooter extends React.Component {

    render () {
        return (
            <footer className={this.getClass()}>
                <div className="container">Footer</div>
            </footer>
        )
    }

    getClass () {
        return classNames(this.props.className, 'footer');
    }
}

AppFooter.propTypes = {
    className: PropTypes.string
};


export default AppFooter;