import React from 'react';

import './page-not-found.scss';

class PageNotFound extends React.Component {

    render () {
        return (
            <div className="page-not-found">
                <div className="page-not-found--content container">
                    <h1>404</h1>
                    <p><strong>File not found</strong></p>
                    <p>The site configured at this address does not
                    contain the requested file.</p>
                </div>
            </div>
        );
    }
}

export default PageNotFound;
