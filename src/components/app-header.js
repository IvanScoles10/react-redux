import classNames from 'classnames';
import NavigationLink from '../core/navigation-link';
import PropTypes from 'prop-types';
import React from 'react';

class AppHeader extends React.Component {

    render () {
        return (
            <header>
                <nav className={this.getClass()}>
                    <a className="navbar-brand" href="#">MS React Redux</a>
                    <button { ...this.getButtonProps()}>
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div {...this.getNavBarCollapseProps()}>
                        <ul className="navbar-nav mr-auto">
                            <NavigationLink to="/users">
                                <span>Users</span>
                            </NavigationLink>
                        </ul>
                    </div>
                </nav>
            </header>
        );
    }

    getClass () {
        return classNames(this.props.className, {
            'navbar': true,
            'navbar-expand-md': true,
            'navbar-dark': true,
            'fixed-top': true,
            'bg-dark': true
        });
    }

    getButtonProps () {
        return {
            'aria-controls': 'navbarCollapse',
            'aria-expanded': 'false',
            'aria-label': 'Toggle navigation',
            className: 'navbar-toggler',
            'data-toggle': 'collapse',
            'data-target': '#navbarCollapse',
            type: 'button'
        };
    }

    getNavBarCollapseProps () {
        return {
            className: 'collapse navbar-collapse',
            id: 'navbarCollapse'
        };
    }
}

AppHeader.propTypes = {
    className: PropTypes.string
};

export default AppHeader;
