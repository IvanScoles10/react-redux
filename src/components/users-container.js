import React from 'react';
import UserForm from '../containers/user-form';
import UsersList from '../containers/users-list';
import './users-container.scss';

class UsersContainer extends React.Component {

    render () {
        return (
            <div className="users-container">
                <UserForm className="users-container--form" />
                <UsersList className="users-container--list" />
            </div>
        )
    }

}

export default UsersContainer;