import PropTypes from 'prop-types';
import React from 'react';
import AppHeader from './app-header';
import AppFooter from './app-footer';
import './app-layout.scss';

class AppLayout extends React.Component {

    constructor (props) {
        super(props);
    }

    render () {
        return (
            <div className="app-layout">
                <AppHeader className="app-layout--header" />
                <main className="app-layout--main" role="main">
                    <section className="app-layout--main-section">
                        <div className="app-layout--main-container container">
                            {this.props.children}
                        </div>
                    </section>
                </main>
                <AppFooter className="app-layout--footer" />
            </div>
        )
    }
}

AppLayout.propTypes = {
    children: PropTypes.node.isRequired
};


export default AppLayout;
