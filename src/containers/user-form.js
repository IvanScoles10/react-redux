import Button from '../core/button';
import classNames from 'classnames';
import Form from '../core/form';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { addUser } from '../actions/index';

const mapDispatchToProps = dispatch => {
    return {
        addUser: user => dispatch(addUser(user))
    };
};

class UserForm extends React.Component {

    constructor () {
        super();

        this.state = {
            first_name: '',
            last_name: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render () {
        return (
            <div className={this.getClass()}>
                <Form {...this.getFormProps()}>
                    <div className="form-group">
                        <label {...this.getLabelProps('first_name')}>First name</label>
                        <input {...this.getInputProps('first_name')} />
                        <div className="feedback-first_name" />
                    </div>
                    <div className="form-group">
                        <label {...this.getLabelProps('last_name')}>Last name</label>
                        <input {...this.getInputProps('last_name')} />
                        <div className="feedback-last_name" />
                    </div>
                   <Button {...this.getSubmitButtonProps()}>Add</Button>
                </Form>
            </div>
        )
    }

    getFormProps () {
        return {
            className: 'needs-validation',
            submit: this.handleSubmit
        };
    }

    getLabelProps (type) {
        return {
            className: 'label-' + type,
            htmlFor: type
        };
    }

    getInputProps (type) {
        let { first_name, last_name } = this.state;

        let props = {
            'first_name': {
                className: 'form-control',
                id: 'first_name',
                name: 'first_name',
                onChange: this.handleChange,
                placeholder: 'First name',
                required: true,
                type: 'text',
                value: first_name
            },
            'last_name': {
                className: 'form-control',
                id: 'last_name',
                name: 'last_name',
                onChange: this.handleChange,
                placeholder: 'Last name',
                required: true,
                type: 'text',
                value: last_name
            }
        };

        return props[type];
    }

    getClass () {
        return classNames(this.props.className, 'user-form');
    }

    getSubmitButtonProps () {
        return {
            type: 'submit'
        };
    }

    handleChange (event) {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit () {
        const { first_name, last_name } = this.state;

        this.props.addUser({
            first_name: first_name,
            last_name: last_name
        }, function () {
            this.setState({
                first_name: '',
                last_name: ''
            });
        });
    }

}

UserForm.propTypes = {
    addUser: PropTypes.func.isRequired,
    className: PropTypes.string
};

export default connect(null, mapDispatchToProps)(UserForm);
