import classNames from 'classnames';
import Button from '../core/button';
import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { deleteUser } from '../actions/index';

const mapDispatchToProps = dispatch => {
    return {
        deleteUser: id => dispatch(deleteUser(id))
    };
};

const mapStateToProps = state => {
    return {
        users: state.users
    };
};

class UsersList extends React.Component {

    constructor () {
        super();

        this.deleteUser = this.deleteUser.bind(this);
    }

    render () {
        return (
            <ul className={this.getClass()}>
                {this.props.users.map(user => (
                    <li className="users--item list-group-item" key={user.id}>
                        {user.last_name}, {user.first_name}
                        <Button {...this.getButtonProps(user.id)}>Remove</Button>
                    </li>
                ))}
            </ul>
        );
    }

    getClass () {
        return classNames(this.props.className, {
            'users-list': true,
            'list-group': true
        });
    }

    getButtonProps (id) {
        return {
            color: 'danger',
            id: id,
            position: 'right',
            size: 'small',
            handleButtonClick: this.deleteUser
        };
    }

    deleteUser (event) {
        event.preventDefault();

        this.props.deleteUser(parseInt(event.target.id), function () {
            console.log('user removed', parseInt(event.target.id));
        });
    }
}

UsersList.propTypes = {
    className: PropTypes.string,
    deleteUser: PropTypes.func.isRequired,
    users: PropTypes.array.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
