import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import AppLayout from './components/app-layout';
import UsersContainer from './components/users-container';
import PageNotFound from './components/page-not-found';
import { Route, browserHistory, Redirect } from 'react-router';
import { BrowserRouter, Switch } from 'react-router-dom';
import store from './storage/store';

import './index.scss';

function renderLayout (Component, Layout) {
    return (<Layout><Component /></Layout>);
}

render (
    <Provider store={store}>
        <BrowserRouter history={browserHistory}>
            <Switch>
                <Route path="/" render={() => renderLayout(UsersContainer, AppLayout)} exact />
                <Route path="/users" render={() => renderLayout(UsersContainer, AppLayout)} exact />
                <Route render={({ history: { location: { pathname, search, hash } } }) => (
                  pathname.slice(-1) === '/' ?
                    <Redirect to={`${pathname.slice(0, -1)}${search}${hash}`} /> :
                    <PageNotFound />
                )} />
            </Switch>
        </BrowserRouter>
    </Provider>,
    document.getElementById('app')
)
