import {
    ADD_USER,
    DELETE_USER,
    EDIT_USER
} from '../constants/action-types';

const initialState = [{
    id: 1,
    first_name: 'Ivan',
    last_name: 'Scoles'
}];

export default function users(state = initialState, action) {
    switch (action.type) {
        case ADD_USER:
            return [
                ...state,
                {
                    id: state.reduce((maxId, user) => Math.max(user.id, maxId), -1) + 1,
                    first_name: action.user.first_name,
                    last_name: action.user.last_name
                }
            ];

        case DELETE_USER:
            return state.filter(user =>
                user.id !== action.id
            )

        case EDIT_USER:
            return state.map(user =>
                user.id === action.id ?
                { 
                    ...user, 
                    first_name: action.user.first_name,
                    last_name: action.user.last_name
                } : user
            );
        default:
            return state
    }
}